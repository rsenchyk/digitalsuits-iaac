data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

data "aws_iam_policy" "AmazonSSMManagedInstanceCore" {
  arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

data "aws_iam_policy" "AmazonEC2RoleforSSM" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}
resource "aws_iam_role" "ec2_role" {
  name               = "${var.role_name}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
resource "aws_iam_role_policy_attachment" "SSM_role_managed_instace_policy_attach" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = data.aws_iam_policy.AmazonSSMManagedInstanceCore.arn
}

resource "aws_iam_role_policy_attachment" "SSM_role_for_ec2_policy_attach" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = data.aws_iam_policy.AmazonEC2RoleforSSM.arn
}

resource "aws_security_group" "cms_sg" {
  name        = var.cms_sg_name
  description = "Allows Web inbound traffic"
  vpc_id      = var.vpc_id
  ingress = [
    {
      description      = "HTTP"
      protocol         = "tcp"
      from_port        = 80
      to_port          = 80
      cidr_blocks      = ["${var.default_route_cidr_block}"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  #If you select a protocol of -1 (semantically equivalent to all)
  #you must specify a from_port and to_port equal to 0.
  egress = [
    {
      description      = "egress trafic"
      protocol         = "-1"
      from_port        = 0
      to_port          = 0
      cidr_blocks      = ["${var.default_route_cidr_block}"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
}

resource "aws_instance" "cms_ec2" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  tags = {
    Name = "${var.product}-${var.project1}-${var.environment}-EC2"
  }
  count                       = var.ec2_count
  subnet_id                   = var.public_subnet_ids[0]
  associate_public_ip_address = true
  vpc_security_group_ids      = ["${aws_security_group.cms_sg.id}"]

  root_block_device {
    delete_on_termination = true
    tags = {
      Name    = "${var.product}-${var.project1}-${var.environment}-EBS"
      Region  = var.region
      Project = var.project1
    }
    volume_size = var.volume_size
    volume_type = var.volume_type
  }
}
