#We need subnet_group, +
#security_group, +
#db_instance(depende on subnet_group),
#parameter_group +- wtf
resource "random_password" "password" {
  length  = 16
  special = false
}
resource "random_string" "username" {
  length  = 12
  special = false
  number  = false
  upper   = false
}

resource "aws_db_subnet_group" "db_subnet_group" {
  name = "database-subnet"
  subnet_ids = var.private_subnet_ids
  description = "subnet for database instance"
}

resource "aws_db_parameter_group" "rds_parameter_group" {
    name = "rds-pg"
    family = "postgres13"
    
   #Some parameters are expected here
}
resource "aws_security_group" "rds_sg" {
  tags = {
    Name = "terraform-example-rds-security-group"
  }
  name        = "postgres_rds_sg"
  description = "Postesql rds security group"
  vpc_id      = var.vpc_id

  ingress {
    from_port       = 5432
    to_port         = 5432
    protocol        = "tcp"
    # cidr_blocks = ["${var.cms_public_cidr_block_a}"]
    cidr_blocks = ["0.0.0.0/0"]
    # cidr_blocks = ["10.0.0.0/28"]
    # cidr_blocks = ["91.234.25.222/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${var.default_route_cidr_block}"]
  }

}

resource "aws_db_instance" "education" {
  identifier             = "education"
  instance_class         = "db.t3.micro"
  allocated_storage      = 5
  engine                 = "postgres"
  engine_version         = "13.1"
  username               = "${random_string.username.result}"
  password               = "${random_password.password.result}"
  db_subnet_group_name   = aws_db_subnet_group.db_subnet_group.name
  vpc_security_group_ids = [aws_security_group.rds_sg.id]
  parameter_group_name   = aws_db_parameter_group.rds_parameter_group.name
  publicly_accessible    = false
  skip_final_snapshot    = true

}
