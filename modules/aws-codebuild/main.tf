resource "aws_security_group" "codebuild_sg" {
  name = var.codebuild_sg_name
  description = var.codebuild_sg_description
  vpc_id = var.vpc_id
  ingress = [
    {
      description      = "HTTP"
      protocol         = "tcp"
      from_port        = 80
      to_port          = 80
      cidr_blocks      = ["${var.default_route_cidr_block}"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
resource "aws_iam_role" "codebuild_role" {
  name = var.codebuild_iam_role

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codebuild_policy" {
  role = aws_iam_role.codebuild_role.name

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": [
        "*"
      ],
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:CreateNetworkInterface",
        "ec2:DescribeDhcpOptions",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeSubnets",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeVpcs"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:CreateNetworkInterfacePermission"
      ],
      "Resource": [
        "arn:aws:ec2:us-east-1:123456789012:network-interface/*"
      ],
      "Condition": {
        "StringEquals": {
          "ec2:Subnet": [
            "${var.public_subnet_a.arn}",
            "${var.public_subnet_b.arn}"
          ],
          "ec2:AuthorizedService": "codebuild.amazonaws.com"
        }
      }
    },
    {
      "Effect": "Allow", 
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "${var.frontend_s3.arn}",
        "${var.frontend_s3.arn}/*"
      ]
    }
  ]
}
POLICY
}

resource "aws_codebuild_project" "codebuild_project" {
  name          = var.codebuild_project_name
  description   = var.codebuild_project_description
  build_timeout = var.codebuild_timeout
  service_role  = aws_iam_role.codebuild_role.arn

  artifacts {
    type = var.type_codebuild_artifacts
  }

  cache {
    type     = var.type_codebuild_cache
    location = var.codebuild_cache_location
  }

  environment {
    compute_type                = var.compute_type 
    image                       = var.codebuild_image
    type                        = var.codebuild_type
    image_pull_credentials_type = var.image_pull_cre_type

  }

  source {
    type            = var.source_type
  }

  source_version = var.environment

  vpc_config {
    vpc_id = var.vpc_id

    subnets = "${var.public_subnet_ids}"

    security_group_ids = ["${aws_security_group.codebuild_sg.id}"]
  }

}
