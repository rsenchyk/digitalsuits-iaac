variable "codebuild_iam_role" {}
variable "public_subnet_ids" {}
variable "public_subnet_a" {}
variable "public_subnet_b" {}
variable "vpc_id" {}
variable "compute_type" {}
variable "codebuild_image" {}
variable "codebuild_type" {}
variable "image_pull_cre_type" {}
variable "source_type" {}
variable "frontend_s3" {}
variable "codebuild_timeout" {}
variable "codebuild_project_description" {}
variable "environment" {}
variable "type_codebuild_cache" {}
variable "type_codebuild_artifacts"{}
variable "codebuild_sg_name" {}
variable "codebuild_sg_description"{}
variable "default_route_cidr_block"{}
variable "codebuild_project_name"{}
variable "codebuild_cache_location"{}