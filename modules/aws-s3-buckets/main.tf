resource "aws_s3_bucket" "frontend_s3" {
  bucket = var.frontend_s3_bucket 
  acl    = "public-read"
  website {
    index_document = "index.html"
    error_document = "error.html"
  }
  tags = {
    Name = "${var.product}-${var.project2}-${var.environment}"
  }
}

resource "aws_s3_bucket" "cms-s3" {
  bucket        = var.cms_s3_bucket
  acl           = "public-read"
  force_destroy = true
  versioning {
    enabled = false
  }
  tags = {
    Name = "${var.product}-${var.project1}-${var.environment}"
  }

}


resource "aws_s3_bucket_policy" "frontend-s3-policy" {
  bucket = aws_s3_bucket.frontend_s3.id
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid       = "PublicRead"
        Effect    = "Allow"
        Principal = "*"
        Action = [
          "s3:GetObject",
          "s3:GetObjectVersion"
        ]
        Resource = [
          "${aws_s3_bucket.frontend_s3.arn}/*"
        ]
      },
    ]
  })
}

resource "aws_s3_bucket_policy" "cms-s3-policy" {
  bucket = aws_s3_bucket.cms-s3.id
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid       = "PublicRead"
        Effect    = "Allow"
        Principal = "*"
        Action = [
          "s3:GetObject",
          "s3:GetObjectVersion"
        ]
        Resource = [
          "${aws_s3_bucket.cms-s3.arn}/*"
        ]
      },
    ]
  })
}