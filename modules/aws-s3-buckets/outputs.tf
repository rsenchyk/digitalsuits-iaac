output "frontend_s3" {
  description = "frontend s3 bucket"
  value       = "${aws_s3_bucket.frontend_s3}"
}