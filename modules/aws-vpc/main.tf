data "aws_availability_zones" "available" {}

resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    "Name" = "${var.product}-${var.environment}"
  }
  enable_dns_support   = true
  enable_dns_hostnames = true
}

resource "aws_subnet" "public_subnet_a" {
  tags = {
    "Name" = "${var.product}-${var.environment}-public-${data.aws_availability_zones.available.names[0]}"
  }
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cms_public_cidr_block_a
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]
}
resource "aws_subnet" "public_subnet_b" {
  tags = {
    "Name" = "${var.product}-${var.environment}-public-${data.aws_availability_zones.available.names[1]}"
  }
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cms_public_cidr_block_b
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[1]
}

resource "aws_subnet" "private_subnet_a" {
  tags = {
    "Name" = "${var.product}-${var.environment}-private-${data.aws_availability_zones.available.names[0]}"
  }
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.cms_private_cidr_block_a
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_subnet" "private_subnet_b" {
  tags = {
    "Name" = "${var.product}-${var.environment}-private-${data.aws_availability_zones.available.names[1]}"
  }
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.cms_private_cidr_block_b
  availability_zone = data.aws_availability_zones.available.names[1]
}

resource "aws_route_table" "main_route_table" {
  vpc_id = aws_vpc.vpc.id

}

resource "aws_route_table_association" "app_route_association" {
  subnet_id      = aws_subnet.public_subnet_a.id
  route_table_id = aws_route_table.main_route_table.id
}

resource "aws_internet_gateway" "my_ig" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route" "route_to_gateway" {
  route_table_id         = aws_route_table.main_route_table.id
  destination_cidr_block = var.default_route_cidr_block
  gateway_id             = aws_internet_gateway.my_ig.id
}