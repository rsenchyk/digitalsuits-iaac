variable "vpc_cidr_block" {
  default = null
}
variable "cms_public_cidr_block_a" {
  default = null
}
variable "cms_public_cidr_block_b" {
  default = null
}

variable "cms_private_cidr_block_a" {
  default = null
}

variable "cms_private_cidr_block_b" {
  default = null
}

variable "default_route_cidr_block" {
  default = null
}
variable "environment" {}
variable "product" {}
