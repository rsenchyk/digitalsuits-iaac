output "public_subnet_ids" {
  description = "List of the public subnets"
  value       = ["${aws_subnet.public_subnet_a.id}", "${aws_subnet.public_subnet_b.id}"]
}
output "private_subnet_ids" {
  description = "List of the private subnets"
  value       = ["${aws_subnet.private_subnet_a.id}", "${aws_subnet.private_subnet_b.id}"]
}
output "vpc_id" {
  description = "id of vpc"
  value       = aws_vpc.vpc.id
}
output "public_subnet_a" {
  description = "public_subnet_a"
  value       = aws_subnet.public_subnet_a
}
output "public_subnet_b" {
  description = "public_subnet_a"
  value       = aws_subnet.public_subnet_b
}
