
resource "aws_cloudfront_distribution" "frontend_s3_distribution" {
    
  origin {
    domain_name = "${var.frontend_s3_bucket}.s3-website.${var.region}.amazonaws.com"
    origin_id = var.frontend_s3_bucket
  }


  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Cloudfront distribution for frontend s3 bucket"
  default_root_object = "index.html" 

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = var.frontend_s3_bucket

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  
  price_class = "PriceClass_All"
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  viewer_certificate {
    cloudfront_default_certificate = true
  }
}

resource "aws_cloudfront_distribution" "cms_s3_distribution" {
  origin {
      domain_name = "${var.cms_s3_bucket}.s3-website.${var.region}.amazonaws.com"
      origin_id = var.cms_s3_bucket
  }


  enabled = true
  is_ipv6_enabled = true
  comment = "Cloudfront distribution for cms s3 bucket"
  default_root_object = "index.html"
#   aliases = ["dev.digitalsuits.co", "cms.dev.digitalsuits.co"]
  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = var.cms_s3_bucket

    forwarded_values {
      query_string = false

      cookies {
          forward = "none"
      }
    }
    
    viewer_protocol_policy = "allow-all"
    min_ttl = 0
    default_ttl = 3600
    max_ttl = 86400
    }

    price_class = "PriceClass_All"
    restrictions {
        geo_restriction {
          restriction_type = "none"
        }
    }
    viewer_certificate {
      cloudfront_default_certificate = true
    }

}