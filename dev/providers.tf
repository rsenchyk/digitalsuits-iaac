provider "aws" {
  region = var.region
  shared_credentials_file = "/home/roman/.aws"
  profile = "default"
  default_tags {
    tags = {
      Product     = var.product
      Environment = var.environment
    }
  }

}