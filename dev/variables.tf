variable "region" {
  description = "region where the services are located"
  type        = string
  default     = "us-east-1"
}
variable "environment" {
  description = "environment of the project"
  type        = string
  default     = "dev"
}
variable "product" {
  description = "digitalsuits pruduct"
  type        = string
  default     = "digitalsuits"
}

variable "project1" {
  description = "cms variable for tags"
  type        = string
  default     = "cms"
}
variable "project2" {
  description = "frontend variable for tags"
  type        = string
  default     = "frontend"
}
variable "public_subnet_a" {
  description = "name of the public subnet a"
  default = "public_subnet_a"
}
variable "public_subnet_b" {
  description = "name of the public subnet b"
  default = "public_subnet_b"
}
variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/26"

}
variable "cms_public_cidr_block_a" {
  description = "(Required)The CIDR block for the CMS public Subnet"
  type        = string
  default     = "10.0.0.0/28"

}
variable "cms_public_cidr_block_b" {
  description = "(Required)The CIDR block for the CMS public Subnet"
  type        = string
  default     = "10.0.0.16/28"

}

variable "cms_private_cidr_block_a" {
  description = "(Required)The CIDR block for the CMS private Subnet"
  type        = string
  default     = "10.0.0.32/28"

}

variable "cms_private_cidr_block_b" {
  description = "(Required)The CIDR block for the CMS private Subnet"
  type        = string
  default     = "10.0.0.48/28"

}

variable "cms_sg_name" {
  description = "name of cms security group"
  type        = string
  default     = "cms-security-group"
}

variable "default_route_cidr_block" {
  description = "(Required) default route in routing table to Internet Gateway"
  type        = string
  default     = "0.0.0.0/0"
}

variable "ec2_count" {
  description = "The counter of ec2 cms instances"
  type        = number
  default     = 1

}

variable "instance_type" {
  description = "type of the ec2 instance"
  type        = string
  default     = "t2.micro"
}
variable "volume_size" {
  description = "size of ec2 volume"
  type        = number
  default     = 5
}

variable "volume_type" {
  description = "type of ec2 volume"
  type        = string
  default     = "gp2"
}


variable "role_name" {
  description = "name of the IAM role for ec2 SSM"
  type        = string
  default     = "smm_ec2_role"
}

variable "frontend_domains_aliases" {
  description = "aliases of frontend for cloudfront"
  type        = list(any)
  default     = ["dev.digitalsuits.co", "www.dev.digitalsuits.co"]
}
variable "compute_type" {
  description = "compute type for aws codebuild"
  type = string
  default = "BUILD_GENERAL1_SMALL"
  
}
variable "codebuild_image" {
  description = "image for codebuild"
  type = string
  default = "aws/codebuild/standard:1.0"
}
variable "codebuild_type"{
  description = "type of build"
  type = string
  default = "LINUX_CONTAINER"
}
variable "image_pull_cre_type"{
  description = "type of the pullinng image"
  type = string
  default = "CODEBUILD"
}
variable "source_type" {
  description = "type of the source for the codebuild"
  type = string
  default = "BITBUCKET"  
}
variable "codebuild_iam_role" {
  description = "Name of the codebuild iam role"
  type = string
  default = "codebuild_role"
}
variable "codebuild_project_name" {
  description = "name of the codebuild project"
  type = string 
  default = "codebuild-project"
}
variable "codebuild_timeout" {
  description  = "timeout of building the project on codebuild"
  type = string
  default = "5"
}
variable "codebuild_project_description"{
  description = "description of the codebuild project"
  type = string
  default = "Test codebuild project"
}
variable "type_codebuild_cache"{
  description = "type of the codebuild cache (local, s3, no cache)"
  type = string
  default = "S3"
}
variable "type_codebuild_artifacts"{
  description = "type of the codebuild artifacts"
  type = string
  default = "NO_ARTIFACTS"
}
variable "codebuild_sg_name" {
  description = "name of the codebuild security group"
  type = string
  default = "codebuild_security_group"
}
variable "codebuild_sg_description" {
  description = "description of the codebuild security group"
  type = string
  default = "Security group for CodeBuild"
}
variable "frontend_s3_name"{
  description = "name for codebuild"
  type = string
  default = "frontend_s3_name"
}
variable "codebuild_cache_location" {
  description = "name of s3 for cache location"
  type = string
  default = "frontend_s3"
  
}