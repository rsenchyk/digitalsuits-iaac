
locals {
  frontend_bucket = "${var.product}-${var.project2}-${var.environment}"
  cms_bucket      = "${var.product}-${var.project1}-${var.environment}"
  region          = var.region
  product         = var.product
  project1        = var.project1
  project2        = var.project2
}

module "aws-vpc" {
  source                   = "../modules/aws-vpc/"
  environment              = var.environment
  product                  = local.product
  vpc_cidr_block           = var.vpc_cidr_block
  cms_public_cidr_block_a  = var.cms_public_cidr_block_a
  cms_public_cidr_block_b  = var.cms_public_cidr_block_b
  cms_private_cidr_block_a = var.cms_private_cidr_block_a
  cms_private_cidr_block_b = var.cms_private_cidr_block_b

}
module "aws-ec2-cms" {
  source                   = "../modules/aws-ec2-cms/"
  environment              = var.environment
  cms_sg_name              = var.cms_sg_name
  public_subnet_ids        = module.aws-vpc.public_subnet_ids
  product                  = var.product
  project1                 = var.project1
  default_route_cidr_block = var.default_route_cidr_block
  instance_type            = var.instance_type
  ec2_count                = var.ec2_count
  vpc_id                   = module.aws-vpc.vpc_id
  region                   = local.region
  volume_size              = var.volume_size
  volume_type              = var.volume_type
  role_name                = var.role_name
}
module "aws-s3-buckets" {
  source             = "../modules/aws-s3-buckets/"
  frontend_s3_bucket = local.frontend_bucket
  cms_s3_bucket      = local.cms_bucket
  region             = local.region
  project1           = local.project1
  project2           = local.project2
  environment        = var.environment
  product            = var.product

}

module "aws-postgres-rds" {
  source                   = "../modules/aws-postgres-rds/"
  private_subnet_ids       = module.aws-vpc.private_subnet_ids
  vpc_id                   = module.aws-vpc.vpc_id
  cms_private_cidr_block_a = var.cms_private_cidr_block_a
  cms_public_cidr_block_a  = var.cms_public_cidr_block_a
  default_route_cidr_block = var.default_route_cidr_block

}


module "aws-cloudfront" {
  source             = "../modules/aws-cloudfront"
  frontend_s3_bucket = local.frontend_bucket
  cms_s3_bucket      = local.cms_bucket
  region             = var.region
}

module "aws-codebuild" {
  source = "../modules/aws-codebuild"
  compute_type = var.compute_type
  codebuild_image = var.codebuild_image
  image_pull_cre_type = var.image_pull_cre_type
  source_type = var.source_type
  frontend_s3 = module.aws-s3-buckets.frontend_s3
  codebuild_timeout = var.codebuild_timeout
  codebuild_project_description = var.codebuild_project_description
  environment = var.environment
  codebuild_iam_role = var.codebuild_iam_role
  public_subnet_ids = module.aws-vpc.private_subnet_ids
  type_codebuild_cache = var.type_codebuild_cache
  public_subnet_a = module.aws-vpc.public_subnet_a
  public_subnet_b = module.aws-vpc.public_subnet_b
  codebuild_type = var.codebuild_type
  codebuild_project_name = var.codebuild_project_name
  type_codebuild_artifacts = var.type_codebuild_artifacts
  vpc_id = module.aws-vpc.vpc_id
  codebuild_sg_name = var.codebuild_sg_name
  codebuild_sg_description = var.codebuild_sg_description
  default_route_cidr_block = var.default_route_cidr_block
  codebuild_cache_location = var.codebuild_cache_location
}
